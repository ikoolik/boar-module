if (canvas.tokens.controlled == 0) return ui.notifications.warn('You must select a token.')
if (game.user.targets.size == 0) return ui.notifications.warn('You must have a target.')


const mapDialogTitle = `Feint`
const mapDialogBody = `
<div>
  <p>With a misleading flourish, you leave an opponent unprepared for your real attack. Attempt a Deception check against that opponent’s Perception DC.</p>
  &nbsp;
</div>
`
const critSuccessMessage = target => {
  return `
    <h3 style='border-bottom: 3px solid black'>Feint (💥 Crit Success)</h3>
    <div style='color:#131516; margin-top:4px;'>
      <p>You throw <b>${target.name}’s</b> defenses against you entirely off. The target is @Compendium[pf2e.conditionitems.AJh5ex99aV6VTggg]{flat-footed} against melee attacks that you attempt against it until the end of your next turn.</p>
    </div>
  `
}
const successMessage = target => {
  return `
    <h3 style='border-bottom: 3px solid black'>Feint (✔️ Success)</h3>
    <div style='color:#131516; margin-top:4px;'>
      <p>Your foe is fooled, but only momentarily. <b>${target.name}</b> is @Compendium[pf2e.conditionitems.AJh5ex99aV6VTggg]{flat-footed} against the next melee attack that you attempt against it before the end of your current turn.</p>
    </div>
  `
}
const failMessage = target => {
  return `
    <h3 style='border-bottom: 3px solid black'>Feint (❌ Fail)</h3>
    <div style='color:#131516; margin-top:4px;'>
      <p><b>${target.name}</b> is unaffected.</p>
    </div>
  `
}
const critFailMessage = target => {
  return `
    <h3 style='border-bottom: 3px solid black'>Feint (❌ Crit Fail)</h3>
    <div style='color:#131516; margin-top:4px;'>
      <p>Your feint backfires. You are @Compendium[pf2e.conditionitems.AJh5ex99aV6VTggg]{flat-footed} against melee attacks the target attempts against you until the end of your next turn.</p>
    </div>
  `
}

const rollTitle = 'Feint - Deception Skill Check'
const notes = [...(token.actor.data.data.skills.dec.notes || [])]
const options = ['action:feint', ...token.actor.getRollOptions(['all', 'skill-check', 'deception'])]
const rollSubject = token.actor.data.data.skills.dec
const rollType = 'skill-check'

const getDc = target => {
  return 10 + target.actor.data.data.attributes.perception.totalModifier
}

/** LOGIC BELOW **/
const toChat = content => {
  const chatData = {user: game.user.id, content, speaker: ChatMessage.getSpeaker()}
  ChatMessage.create(chatData, {})
}

const getCritModifierFromRoll = roll => {
  return roll === 1 ? -10 : (roll === 20 ? 10 : 0)
}

const askForMap = (title, content, callback, map = 5) => {
  const buttons = {
    nomap: {label: 'No MAP', callback: () => callback(0)},
    onemap: {label: `-${map}`, callback: () => callback(-1 * map)},
    twomap: {label: `-${map * 2}`, callback: () => callback(-2 * map)}
  }
  const dialog = new Dialog({title, content, buttons})
  dialog.render(true)
}

game.user.targets.forEach(target => {
  const DC = getDc(target)
  
  PF2Check.roll(
    new PF2CheckModifier(rollTitle, rollSubject),
    { actor: token.actor, type: rollType, options, notes },
    event,
    roll => {
      let result = roll._total + getCritModifierFromRoll(roll.parts[0].rolls[0].result)
      switch(true) {
        case (result >= DC + 10):
          toChat(critSuccessMessage(target))
          break
        case (result >= DC) :
          toChat(successMessage(target))
          break
        case (result < DC - 10) :
          toChat(critFailMessage(target))
          break
        default :
          toChat(failMessage(target))
      }
    }
  )
})
