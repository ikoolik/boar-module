if (canvas.tokens.controlled == 0) return ui.notifications.warn('You must select a token.')
if (game.user.targets.size == 0) return ui.notifications.warn('You must have a target.')


const mapDialogTitle = `Grapple`
const mapDialogBody = `
<div>
  <p>You attempt to grab a creature or object with your free hand. Attempt an Athletics check against the target's Fortitude DC. You can Grapple a target you already have grabbed or restrained without having a hand free.</p>
  &nbsp;
</div>
`
const critSuccessMessage = target => {
  const crushingGrabLine = hasFeat('crushing-grab')
    ? `<p style='border-top: 1px solid black'>Like a powerful constrictor, you crush targets in your unyielding grasp. You can deal [[/r ${token.actor.data.data.abilities.str.mod}]] bludgeoning damage to ${target.name}. You can make this attack nonlethal with no penalty.</p>`
    : ``
  return `
    <h3 style='border-bottom: 3px solid black'>Grapple (💥 Crit Success)</h3>
    <div style='color:#131516; margin-top:4px;'>
      <p><b>${target.name}</b> is @Compendium[pf2e.conditionitems.VcDeM8A5oI6VqhbM]{restrained} until the end of your next turn unless you move or your opponent Escapes.</p>
      ${crushingGrabLine}
    </div>
  `
}
const successMessage = target => {
  const crushingGrabLine = hasFeat('crushing-grab')
    ? `<p style='border-top: 1px solid black'>Like a powerful constrictor, you crush targets in your unyielding grasp. You can deal [[/r ${token.actor.data.data.abilities.str.mod}]] bludgeoning damage to ${target.name}. You can make this attack nonlethal with no penalty.</p>`
    : ``
  return `
    <h3 style='border-bottom: 3px solid black'>Grapple (✔️ Success)</h3>
    <div style='color:#131516; margin-top:4px;'>
      <p><b>${target.name}</b> is @Compendium[pf2e.conditionitems.kWc1fhmv9LBiTuei]{grabbed} until the end of your next turn unless you move or your opponent Escapes.</p>
      ${crushingGrabLine}
    </div>
  `
}
const failMessage = target => {
  return `
    <h3 style='border-bottom: 3px solid black'>Grapple (❌ Fail)</h3>
    <div style='color:#131516; margin-top:4px;'>
      <p>You fail to grab <b>${target.name}</b>. If you already had <b>${target.name}</b> grabbed or restrained using a Grapple, those conditions on that creature end.</p>
    </div>
  `
}
const critFailMessage = target => {
  return `
    <h3 style='border-bottom: 3px solid black'>Grapple (❌ Crit Fail)</h3>
    <div style='color:#131516; margin-top:4px;'>
      <p>If you already had <b>${target.name}</b> grabbed or restrained, it breaks free. <b>${target.name}</b> can either grab you and apply @Compendium[pf2e.conditionitems.kWc1fhmv9LBiTuei]{grabbed}, as if it succeeded at using the Grapple action against you, or force you to fall and land @Compendium[pf2e.conditionitems.j91X7x0XSomq8d60]{prone}.</p>
    </div>
  `
}

const rollTitle = 'Grapple - Athletics Skill Check'
const notes = [...(token.actor.data.data.skills.ath.notes || [])]
const options = ['action:grapple', ...token.actor.getRollOptions(['all', 'skill-check', 'athletics'])]
const rollSubject = token.actor.data.data.skills.ath
const rollType = 'skill-check'

const getDc = target => {
  return 10 + target.actor.data.data.saves.fortitude.totalModifier
}

/** LOGIC BELOW **/
const hasFeat = slug => {
  if (token.actor.items.find(i => i.data.data.slug === slug && i.type === 'feat')) {
      return true;
  }
  return false;
}

const toChat = content => {
  const chatData = {user: game.user.id, content, speaker: ChatMessage.getSpeaker()}
  ChatMessage.create(chatData, {})
}

const getCritModifierFromRoll = roll => {
  return roll === 1 ? -10 : (roll === 20 ? 10 : 0)
}

const askForMap = (title, content, callback, map = 5) => {
  const buttons = {
    nomap: {label: 'No MAP', callback: () => callback(0)},
    onemap: {label: `-${map}`, callback: () => callback(-1 * map)},
    twomap: {label: `-${map * 2}`, callback: () => callback(-2 * map)}
  }
  const dialog = new Dialog({title, content, buttons})
  dialog.render(true)
}

game.user.targets.forEach(target => {
  const DC = getDc(target)

  askForMap(mapDialogTitle, mapDialogBody, map => {
    const modifiers = []
    if(map !== 0) {
      modifiers.push(new PF2Modifier('MAP', map, 'untyped'))
    }

    PF2Check.roll(
      new PF2CheckModifier(rollTitle, rollSubject, modifiers),
      { actor: token.actor, type: rollType, options, notes },
      event,
      roll => {
        let result = roll._total + getCritModifierFromRoll(roll.parts[0].rolls[0].result)
        switch(true) {
          case (result >= DC + 10):
            toChat(critSuccessMessage(target))
            break
          case (result >= DC) :
            toChat(successMessage(target))
            break
          case (result < DC - 10) :
            toChat(critFailMessage(target))
            break
          default :
            toChat(failMessage(target))
        }
      }
    )
  })
})
