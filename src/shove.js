if (canvas.tokens.controlled == 0) return ui.notifications.warn('You must select a token.')
if (game.user.targets.size == 0) return ui.notifications.warn('You must have a target.')
//

const mapDialogTitle = `Shove`
const mapDialogBody = `
<div>
  <p>You push a creature away from you. Attempt an Athletics check against your target's Fortitude DC.</p>
  &nbsp;
</div>
`
const critSuccessMessage = target => {
  return `
    <h3 style='border-bottom: 3px solid black'>Shove (💥 Crit Success)</h3>
    <div style='color:#131516; margin-top:4px;'>
      <p>You push <b>${target.name}</b> up to 10 feet away from you. You can Stride after it, but you must move the same distance and in the same direction.</p>
    </div>
  `
}
const successMessage = target => {
  return `
    <h3 style='border-bottom: 3px solid black'>Shove (✔️ Success)</h3>
    <div style='color:#131516; margin-top:4px;'>
      <p>You push <b>${target.name}</b> back 5 feet. You can Stride after it, but you must move the same distance and in the same direction.</p>
    </div>
  `
}
const failMessage = target => {
  return `
    <h3 style='border-bottom: 3px solid black'>Shove (❌ Fail)</h3>
    <div style='color:#131516; margin-top:4px;'>
      <p><b>${target.name}</b> is unaffected.</p>
    </div>
  `
}
const critFailMessage = target => {
  return `
    <h3 style='border-bottom: 3px solid black'>Shove (❌ Crit Fail)</h3>
    <div style='color:#131516; margin-top:4px;'>
      <p>You lose your balance, fall, and land @Compendium[pf2e.conditionitems.j91X7x0XSomq8d60]{prone}.</p>
    </div>
  `
}

const rollTitle = 'Shove - Athletics Skill Check'
const notes = [...(token.actor.data.data.skills.ath.notes || [])]
const options = ['action:shove', ...token.actor.getRollOptions(['all', 'skill-check', 'athletics'])]
const rollSubject = token.actor.data.data.skills.ath
const rollType = 'skill-check'

const getDc = target => {
  return 10 + target.actor.data.data.saves.fortitude.totalModifier
}

/** LOGIC BELOW **/
const hasFeat = slug => {
  if (token.actor.items.find(i => i.data.data.slug === slug && i.type === 'feat')) {
      return true;
  }
  return false;
}

const toChat = content => {
  const chatData = {user: game.user.id, content, speaker: ChatMessage.getSpeaker()}
  ChatMessage.create(chatData, {})
}

const getCritModifierFromRoll = roll => {
  return roll === 1 ? -10 : (roll === 20 ? 10 : 0)
}

const askForMap = (title, content, callback, map = 5) => {
  const buttons = {
    nomap: {label: 'No MAP', callback: () => callback(0)},
    onemap: {label: `-${map}`, callback: () => callback(-1 * map)},
    twomap: {label: `-${map * 2}`, callback: () => callback(-2 * map)}
  }
  const dialog = new Dialog({title, content, buttons})
  dialog.render(true)
}

game.user.targets.forEach(target => {
  const DC = getDc(target)

  askForMap(mapDialogTitle, mapDialogBody, map => {
    const modifiers = []
    if(map !== 0) {
      modifiers.push(new PF2Modifier('MAP', map, 'untyped'))
    }

    PF2Check.roll(
      new PF2CheckModifier(rollTitle, rollSubject, modifiers),
      { actor: token.actor, type: rollType, options, notes },
      event,
      roll => {
        let result = roll._total + getCritModifierFromRoll(roll.parts[0].rolls[0].result)
        switch(true) {
          case (result >= DC + 10):
            toChat(critSuccessMessage(target))
            break
          case (result >= DC) :
            toChat(successMessage(target))
            break
          case (result < DC - 10) :
            toChat(critFailMessage(target))
            break
          default :
            toChat(failMessage(target))
        }
      }
    )
  })
})
