if (canvas.tokens.controlled == 0) return ui.notifications.warn('You must select a token.')
if (game.user.targets.size == 0) return ui.notifications.warn('You must have a target.')

const mapDialogTitle = `Trip`
const mapDialogBody = `
<div>
  <p>You try to knock a creature to the ground. Attempt an Athletics check against the target’s Reflex DC.</p>
  &nbsp;
</div>
`
const critSuccessMessage = target => {
  return `
    <h3 style='border-bottom: 3px solid black'>Trip (💥 Crit Success)</h3>
    <div style='color:#131516; margin-top:4px;'>
      <p><b>${target.name}</b> falls, lands @Compendium[pf2e.conditionitems.j91X7x0XSomq8d60]{prone} and takes [[/r 1d6]] bludgeoning damage.</p>
    </div>
  `
}
const successMessage = target => {
  return `
    <h3 style='border-bottom: 3px solid black'>Trip (✔️ Success)</h3>
    <div style='color:#131516; margin-top:4px;'>
      <p><b>${target.name}</b> The target falls and lands @Compendium[pf2e.conditionitems.j91X7x0XSomq8d60]{prone}.</p>
    </div>
  `
}
const failMessage = target => {
  return `
    <h3 style='border-bottom: 3px solid black'>Trip (❌ Fail)</h3>
    <div style='color:#131516; margin-top:4px;'>
      <p><b>${target.name}</b> is unharmed.</p>
    </div>
  `
}
const critFailMessage = target => {
  return `
    <h3 style='border-bottom: 3px solid black'>Trip (❌ Crit Fail)</h3>
    <div style='color:#131516; margin-top:4px;'>
      <p><b>${target.name}</b> is unharmed. You lose your balance and fall and land @Compendium[pf2e.conditionitems.j91X7x0XSomq8d60]{prone}.</p>
    </div>
  `
}

const rollTitle = 'Trip - Athletics Skill Check'
const notes = [...(token.actor.data.data.skills.ath.notes || [])]
const options = ['action:trip', ...token.actor.getRollOptions(['all', 'skill-check', 'athletics'])]
const rollSubject = token.actor.data.data.skills.ath
const rollType = 'skill-check'

const getDc = target => {
  return 10 + target.actor.data.data.saves.reflex.totalModifier
}

/** LOGIC BELOW **/
const toChat = content => {
  const chatData = {user: game.user.id, content, speaker: ChatMessage.getSpeaker()}
  ChatMessage.create(chatData, {})
}

const getCritModifierFromRoll = roll => {
  return roll === 1 ? -10 : (roll === 20 ? 10 : 0)
}

const askForMap = (title, content, callback, map = 5) => {
  const buttons = {
    nomap: {label: 'No MAP', callback: () => callback(0)},
    onemap: {label: `-${map}`, callback: () => callback(-1 * map)},
    twomap: {label: `-${map * 2}`, callback: () => callback(-2 * map)}
  }
  const dialog = new Dialog({title, content, buttons})
  dialog.render(true)
}

game.user.targets.forEach(target => {
  const DC = getDc(target)

  askForMap(mapDialogTitle, mapDialogBody, map => {
    const modifiers = []
    if(map !== 0) {
      modifiers.push(new PF2Modifier('MAP', map, 'untyped'))
    }

    PF2Check.roll(
      new PF2CheckModifier(rollTitle, rollSubject, modifiers),
      { actor: token.actor, type: rollType, options, notes },
      event,
      roll => {
        let result = roll._total + getCritModifierFromRoll(roll.parts[0].rolls[0].result)
        switch(true) {
          case (result >= DC + 10):
            toChat(critSuccessMessage(target))
            break
          case (result >= DC) :
            toChat(successMessage(target))
            break
          case (result < DC - 10) :
            toChat(critFailMessage(target))
            break
          default :
            toChat(failMessage(target))
        }
      }
    )
  })
})
